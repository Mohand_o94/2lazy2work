import '../css/bootstrap.css';
import '../css/style.css';
import '../css/responsive.css';
import '../css/font-awesome.min.css';
import Head from 'next/head'
import { appWithTranslation } from 'next-i18next'
import Layout from '../components/Layout';


function MyApp({ Component, pageProps, router }) {

  return (
    <>
      <Head>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        {/* <!-- Mobile Metas --> */}
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

          <title> 2Lazy2Work </title>
      </Head>


      <div className={router.route !== '/' ? 'sub_page' : ''}>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </div>
      {/* <!-- jQery --> */}
      <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
      {/* <!-- popper js --> */}
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossOrigin="anonymous">
      </script>
      {/* <!-- bootstrap js --> */}
      <script type="text/javascript" src="js/bootstrap.js"></script>
      {/* <!-- owl slider --> */}
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js">
      </script>
      {/* <!-- custom js --> */}
      <script type="text/javascript" src="js/custom.js"></script>
  
    </>
  )
}




export default appWithTranslation(MyApp);
