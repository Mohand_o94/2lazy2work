import { newsletterRepo } from "../../../helpers/newsletter-repo"
export default function get(req, res) {
  const { query } = req;
  if (query.pwd === 'chuuut') {
    const list = newsletterRepo.getAll()
    res.status(200).json(list)
  } else {
    res.status(401).json({ msg: 'forbidden'})
  }
  
}
