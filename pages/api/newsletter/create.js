import { newsletterRepo } from "../../../helpers/newsletter-repo"
export default function create(req, res) {
  const { query } = req;
  newsletterRepo.create(query.email)
  res.status(200).json({ res: `${query.email} added` })
}
