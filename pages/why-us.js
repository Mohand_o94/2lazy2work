import Head from 'next/head'
import Why from '../components/Why';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

export default function why() {
    return (
        <>
            <Head>
                <title>Why hire us - 2Lazy2Work Web Agency based in Montreal</title>
                <meta name="description" content="Why hire us - 2Lazy2Work Web Agency based in Montreal. What we do is create, we seek to find the most effective solutions to the technological problems of tomorrow" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

           <Why />
        </>
    )
}

export const getStaticProps = async({locale}) => {

    return {
        props: {
          ...(await serverSideTranslations(locale, [
            'common'
          ])),
          // Will be passed to the page component as props
        },
      }
  }