import About from '../components/About';
import Client from '../components/Client';
import Team from '../components/Team';
import Services from '../components/Services';
import Why from '../components/Why';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

export default function Home(props) {

    return (
        <>
            <Services />
            <About />
            <Why />
            <Team />
            <Client />
        </>
    );
}


export const getStaticProps = async ({locale}) => {

    return {
        props: {
          ...(await serverSideTranslations(locale, [
            'common'
          ])),
          // Will be passed to the page component as props
        },
      }
}