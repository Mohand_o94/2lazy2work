const { i18n } = require('./next-i18next.config')

module.exports = {
  i18n,
  reactStrictMode: true,
  images: {
    domains: ['images.unsplash.com'],
    domains: ['firebasestorage.googleapis.com'],
  },
  experimental: {
    forceSwcTransforms: true,
  },
}
