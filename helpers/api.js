
export const addEmailNewsletter = async(email) => {

    const resp = await fetch(`/api/newsletter/create?email=${email}`);

    return resp;

}