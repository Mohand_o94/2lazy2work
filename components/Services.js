import React from "react";
import { useTranslation } from 'next-i18next'
import Link from "next/link";
import { useRouter } from 'next/router'


const Services = () => {

    const { t } = useTranslation();
    const router = useRouter();
    const { route } = router;

    return (
        <>
            <section className="service_section layout_padding" id="services">
                <div className="service_container">
                    <div className="container ">
                        <div className="heading_container heading_center">
                            <h2>
                                {t('our')} <span>{t('services')}</span>
                            </h2>
                            <p>
                                {t('servicesDesc')}
                            </p>
                        </div>
                        <div className="row">
                            <div className="col-md-4 ">
                                <div className="box ">
                                    <div className="img-box">
                                        <img src="/images/web.png" alt="" />
                                    </div>
                                    <div className="detail-box">
                                        <h5>{
                                            t('webDev')}
                                        </h5>
                                        <p>
                                            {t('webDevDesc')}
                                        </p>
                                        <a href="">
                                            {t('readMore')}
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 ">
                                <div className="box ">
                                    <div className="img-box">
                                        <img src="/images/think.png" alt="" />
                                    </div>
                                    <div className="detail-box">
                                        <h5>
                                            {t('webDesign')}
                                        </h5>
                                        <p>
                                            {t('webDesignDesc')}
                                        </p>

                                        <a href="">
                                            {t('readMore')}
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 ">
                                <div className="box ">
                                    <div className="img-box">
                                        <img src="/images/s3.png" alt="" />
                                    </div>
                                    <div className="detail-box">
                                        <h5>
                                            {t('support')}
                                        </h5>
                                        <p>
                                            {t('supportDesc')}
                                        </p>

                                        <a href="">
                                            {t('readMore')}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="btn-box">
                           { route === '/' && <Link href={'/services'}><a>
                                {t('viewAll')}
                            </a></Link>}
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
}

export default Services;