import React, { useState } from "react";
import Link from "next/link";
import { useRouter } from 'next/router'
import { useTranslation } from 'next-i18next'


const Header = () => {
    const { t } = useTranslation();
    const router = useRouter();
    const { route } = router;
    return (
        <>
            <div className="hero_area">

                <div className="hero_bg_box">
                    <div className="bg_img_box">
                        <img src="/images/hero-bg.png" alt="" />
                    </div>
                </div>

                <header className="header_section">
                    <div className="container-fluid">
                        <nav className="navbar navbar-expand-lg custom_nav-container ">
                            <Link href="/"><a className="navbar-brand">
                                <span>
                                    {t('brand')}
                                </span>
                            </a></Link>

                            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span className=""> </span>
                            </button>

                            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul className="navbar-nav  ">
                                    <li className={`nav-item ${route === '/' && 'active'}`}>
                                        <Link href="/"><a className="nav-link">{t('home')}</a></Link>
                                    </li>
                                    <li className={`nav-item ${route === '/about' && 'active'}`}>
                                        <Link href="/about"><a className="nav-link">{t('about')}</a></Link>
                                    </li>
                                    <li className={`nav-item ${route === '/services' && 'active'}`}>
                                        <Link href="/services"><a className="nav-link">{t('services')}</a></Link>
                                    </li>
                                    <li className={`nav-item ${route === '/why-us' && 'active'}`}>
                                        <Link href="/why-us"><a className="nav-link">{t('whyUs')}</a></Link>
                                    </li>
                                    <li className={`nav-item ${route === '/team' && 'active'}`}>
                                        <Link href="/team"><a className="nav-link">{t('team')}</a></Link>
                                    </li>
                                    <li className={`nav-item`}>
                                        <button className="btn btn-secondary nav-link" type="button" onClick={() => { router.push(route, route, { locale: t('lang') === 'Fr' ? 'fr' : 'en' }) }}>
                                            {t('lang')}
                                        </button>
                                    </li>
                                    <form className="form-inline">
                                        <button className="btn  my-2 my-sm-0 nav_search-btn" type="submit">
                                            <i className="fa fa-search" aria-hidden="true"></i>
                                        </button>
                                    </form>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </header>
                {route === '/' &&
                    <section className="slider_section ">
                        <div id="customCarousel1" className="carousel slide" data-ride="carousel">
                            <div className="carousel-inner">
                                <div className="carousel-item active">
                                    <div className="container ">
                                        <div className="row">
                                            <div className="col-md-6 ">
                                                <div className="detail-box">
                                                    <h1>
                                                        {t('blogTitle1')}
                                                    </h1>
                                                    <p>
                                                        {t('blogDesc1')}
                                                    </p>
                                                    <div className="btn-box">
                                                        <a href="https://learn.microsoft.com/fr-fr/visualstudio/javascript/tutorial-nodejs-with-react-and-jsx?view=vs-2022" className="btn1">
                                                            {t('readMore')}
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                <div className="img-box">
                                                    <img src="images/world.png" alt="" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="carousel-item ">
                                    <div className="container ">
                                        <div className="row">
                                            <div className="col-md-6 ">
                                                <div className="detail-box">
                                                    <h1>
                                                        {t('blogTitle2')}

                                                    </h1>
                                                    <p>
                                                        {t('blogDesc2')}

                                                    </p>
                                                    <div className="btn-box">
                                                        <a href="https://www.youtube.com/watch?v=mnuUz9CpE_4" className="btn1">
                                                            {t('readMore')}
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                <div className="img-box">
                                                    <img src="images/technology.png" alt="" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="carousel-item">
                                    <div className="container ">
                                        <div className="row">
                                            <div className="col-md-6 ">
                                                <div className="detail-box">
                                                    <h1>
                                                        {t('blogTitle3')}

                                                    </h1>
                                                    <p>
                                                        {t('blogDesc3')}
                                                    </p>
                                                    <div className="btn-box">
                                                        <a href="https://prismic.io/blog/vercel-vs-netlify" className="btn1">
                                                            {t('readMore')}
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                <div className="img-box">
                                                    <img src="images/devops.png" alt="" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ol className="carousel-indicators">
                                <li data-target="#customCarousel1" data-slide-to="0" className="active"></li>
                                <li data-target="#customCarousel1" data-slide-to="1"></li>
                                <li data-target="#customCarousel1" data-slide-to="2"></li>
                            </ol>
                        </div>
                    </section>
                }
            </div>
        </>
    );
}

export default Header;