import Link from "next/link";
import React from "react";
import { useTranslation } from 'next-i18next'
import { useRouter } from 'next/router'
import { addEmailNewsletter } from '../helpers/api'


const Footer = () => {
    const { t } = useTranslation();
    const router = useRouter();
    const { route } = router;

    const [subform, setSubform] = React.useState('');
    const [errorEmail, setErrorEmail] = React.useState('');
    const newsletter = async (event) => {
        setErrorEmail('');
        event.preventDefault();
        const mail = event.target.email.value;
        console.log(mail)
        console.log(validateEmail(mail))
        if (validateEmail(mail)) {
            setSubform(t('thanks'));
            addEmailNewsletter();
            return;
        }
        else {
            setErrorEmail(t('notValid'));
            return;
        }

    }

    const validateEmail = (email) => {
        const emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
        return emailRegex.test(email);
    };

    return (
        <>
            <section className="info_section layout_padding2">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6 col-lg-3 info_col">
                            <div className="info_contact">
                                <h4>
                                    {t('address')}
                                </h4>
                                <div className="contact_link_box">
                                    <a href="">
                                        <i className="fa fa-map-marker" aria-hidden="true"></i>
                                        <span>
                                            200 rue Sainte-Marguerite, H4W 2R4, Montreal, QC Canada
                                        </span>
                                    </a>
                                    <a href="">
                                        <i className="fa fa-phone" aria-hidden="true"></i>
                                        <span>
                                            +1 514-691-9427
                                        </span>
                                    </a>
                                    <a href="">
                                        <i className="fa fa-envelope" aria-hidden="true"></i>
                                        <span>
                                            mohand.oussadi@gmail.com
                                        </span>
                                    </a>
                                </div>
                            </div>
                            <div className="info_social">
                                {/*                                 <a href="#">
                                    <i className="fa fa-facebook" aria-hidden="true"></i>
                                </a>
                                <a href="">
                                    <i className="fa fa-twitter" aria-hidden="true"></i>
                                </a> */}
                                <a href="https://www.linkedin.com/in/mohand-oussadi-091777106/">
                                    <i className="fa fa-linkedin" aria-hidden="true"></i>
                                </a>
                                {/* <a href="">
                                    <i className="fa fa-instagram" aria-hidden="true"></i>
                                </a> */}
                            </div>
                        </div>
                        <div className="col-md-6 col-lg-3 info_col">
                            <div className="info_detail">
                                <h4>
                                    {t('brand')}
                                </h4>

                            </div>
                        </div>
                        <div className="col-md-6 col-lg-2 mx-auto info_col">
                            <div className="info_link_box">
                                <div className="info_links">
                                    <Link href="/"><a className="active">
                                        {t('home')}
                                    </a></Link>
                                    <Link href="/about"><a className=""  >
                                        {t('about')}
                                    </a></Link>
                                    <Link href="/services"><a className=""  >
                                        {t('services')}
                                    </a></Link>
                                    <Link href="/why-us"><a className="" >
                                        {t('whyUs')}
                                    </a></Link>
                                    <Link href="/team"><a className="">
                                        {t('team')}
                                    </a></Link>
                                    <button className="btn btn-secondary nav-link" type="button" onClick={() => { router.push(route, route, { locale: t('lang') === 'Fr' ? 'fr' : 'en' }) }}>
                                        {t('lang')}
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-lg-3 info_col ">
                            <h4>
                                {t('subscribe')}
                            </h4>
                            <form onSubmit={newsletter}>
                                <input type={'email'} id={'email'} placeholder={t('enterMail')} />
                                {subform === '' &&
                                    <button type="submit" >
                                        {t('subscribe')}
                                    </button>}
                            </form>

                            <p>{subform}</p>
                            <p className="error">{errorEmail}</p>
                        </div>
                    </div>
                </div>
            </section>

        </>
    );
}

export default Footer;