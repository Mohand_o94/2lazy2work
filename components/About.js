import React from "react";
import { useRouter } from 'next/router'
import { useTranslation } from 'next-i18next'
import Link from "next/link";

const About = () => {
    const { t } = useTranslation();
    const router = useRouter();
    const { route } = router;
    return (
        <>
            <section className="about_section layout_padding">
                <div className="container  ">
                    <div className="heading_container heading_center">
                        <h2>
                            {t('about')} <span>{t('us')}</span>
                        </h2>
                        <p>
                            {t('brand')} {t('slogan')}
                        </p>
                    </div>
                    <div className="row">
                        <div className="col-md-6 ">
                            <div className="img-box">
                                <img src="/images/connex.png" alt="" />
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="detail-box">
                                <h3>
                                    {t('weAre')} {t('brand')}
                                </h3>
                                <p>
                                    {t('aboutUsDesc')}
                                </p>
                                {route === '/' && <Link href={'/about'}><a >
                                    {t('readMore')}
                                </a></Link>}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
}

export default About;









