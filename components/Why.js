import React from "react";
import { useTranslation } from 'next-i18next'
import Link from "next/link";
import { useRouter } from 'next/router'

const Why = () => {
  const { t } = useTranslation();
  const router = useRouter();
  const { route } = router;
  return (
    <>
      {/* <!-- why section --> */}

      <section className="why_section layout_padding">
        <div className="container">
          <div className="heading_container heading_center">
            <h2>
              {t('whyChoose')} <span>{t('us')}</span>
            </h2>
          </div>
          <div className="why_container">
            <div className="box">
              <div className="img-box">
                <img src="/images/w1.png" alt="" />
              </div>
              <div className="detail-box">
                <h5>
                  {t('expertise')}
                </h5>
                <p>
                  {t('expertiseDesc')}
                </p>
              </div>
            </div>
            <div className="box">
              <div className="img-box">
                <img src="/images/w2.png" alt="" />
              </div>
              <div className="detail-box">
                <h5>
                  {t('customSolutions')}
                </h5>
                <p>
                  {t('customSolutionsDesc')}
                </p>
              </div>
            </div>
            <div className="box">
              <div className="img-box">
                <img src="/images/w3.png" alt="" />
              </div>
              <div className="detail-box">
                <h5>
                  {t('creativity')}
                </h5>
                <p>
                  {t('creativityDesc')}
                </p>
              </div>
            </div>
            <div className="box">
              <div className="img-box">
                <img src="/images/w4.png" alt="" />
              </div>
              <div className="detail-box">
                <h5>
                  {t('communication')}
                </h5>
                <p>
                  {t('communicationDesc')}
                </p>
              </div>
            </div>
          </div>
          <div className="btn-box">
            {route === '/' && <Link href={'/why-us'}><a >
              {t('readMore')}
            </a></Link>}
          </div>
        </div>
      </section>
    </>
  );
}

export default Why;