import React from "react";
import Image from "next/image";
import { useTranslation } from 'next-i18next'

const Team = () => {
    const { t } = useTranslation();

    return (
        <section className="team_section layout_padding">
            {/*  <!-- team section --> */}

            <div className="container-fluid">
                <div className="heading_container heading_center">
                    <h2 className="">
                        {t('my')} <span> {t('team')}</span>
                    </h2>
                </div>

                <div className="team_container">
                    <div className="row">
                        <div className="col-lg-3 col-sm-6">
                            <div className="box ">
                                <div className="img-box">
                                    <img src="/images/mohand.jpg" className="img1" alt="Mohand Oussadi" />
                                </div>
                                <div className="detail-box">
                                    <h5>
                                        Mohand Oussadi
                                    </h5>
                                    <p>
                                        CEO/CTO
                                    </p>
                                </div>
                                <div className="social_box">
                                    {/* <a href="#">
                                        <i className="fa fa-facebook" aria-hidden="true"></i>
                                    </a>
                                    <a href="#">
                                        <i className="fa fa-twitter" aria-hidden="true"></i>
                                    </a> */}
                                    <a href="https://www.linkedin.com/in/mohand-oussadi-091777106/">
                                        <i className="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                   {/*  <a href="#">
                                        <i className="fa fa-instagram" aria-hidden="true"></i>
                                    </a>
                                    <a href="#">
                                        <i className="fa fa-youtube-play" aria-hidden="true"></i>
                                    </a> */}
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-sm-6">
                            <div className="box ">
                                <div className="img-box">
                                    <img src="/images/team-2.jpg" className="img1" alt="" />
                                </div>
                                <div className="detail-box">
                                    <h5>
                                        Allison Repesse
                                    </h5>
                                    <p>
                                        Head Manager
                                    </p>
                                </div>
                                {/* <div className="social_box">
                                    <a href="#">
                                        <i className="fa fa-facebook" aria-hidden="true"></i>
                                    </a>
                                    <a href="#">
                                        <i className="fa fa-twitter" aria-hidden="true"></i>
                                    </a>
                                    <a href="#">
                                        <i className="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                    <a href="#">
                                        <i className="fa fa-instagram" aria-hidden="true"></i>
                                    </a>
                                    <a href="#">
                                        <i className="fa fa-youtube-play" aria-hidden="true"></i>
                                    </a>
                                </div> */}
                            </div>
                        </div>
                        <div className="col-lg-3 col-sm-6">
                            <div className="box ">
                                <div className="img-box">
                                    <img src="/images/team-3.jpg" className="img1" alt="" />
                                </div>
                                <div className="detail-box">
                                    <h5>
                                        Alex Lescat
                                    </h5>
                                    <p>
                                        Product Manager
                                    </p>
                                </div>
                                {/* <div className="social_box">
                                    <a href="#">
                                        <i className="fa fa-facebook" aria-hidden="true"></i>
                                    </a>
                                    <a href="#">
                                        <i className="fa fa-twitter" aria-hidden="true"></i>
                                    </a>
                                    <a href="#">
                                        <i className="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                    <a href="#">
                                        <i className="fa fa-instagram" aria-hidden="true"></i>
                                    </a>
                                    <a href="#">
                                        <i className="fa fa-youtube-play" aria-hidden="true"></i>
                                    </a>
                                </div> */}
                            </div>
                        </div>
                        <div className="col-lg-3 col-sm-6">
                            <div className="box ">
                                <div className="img-box">
                                    <img src="/images/team-4.jpg" className="img1" alt="" />
                                </div>
                                <div className="detail-box">
                                    <h5>
                                        Yanis Oussadi
                                    </h5>
                                    <p>
                                        Developer IT
                                    </p>
                                </div>
                                {/* <div className="social_box">
                                    <a href="#">
                                        <i className="fa fa-facebook" aria-hidden="true"></i>
                                    </a>
                                    <a href="#">
                                        <i className="fa fa-twitter" aria-hidden="true"></i>
                                    </a>
                                    <a href="#">
                                        <i className="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                    <a href="#">
                                        <i className="fa fa-instagram" aria-hidden="true"></i>
                                    </a>
                                    <a href="#">
                                        <i className="fa fa-youtube-play" aria-hidden="true"></i>
                                    </a>
                                </div> */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* <!-- end team section --> */ }
        </section>
    );
}

export default Team;