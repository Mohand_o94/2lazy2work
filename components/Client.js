import React from "react";
import { useTranslation } from 'next-i18next'
const Client = () => {
    const { t } = useTranslation(); 
    return (
        <section className="client_section layout_padding">
            <div className="container">
                <div className="heading_container heading_center psudo_white_primary mb_45">
                    <h2>
                        {t('whatSaysOur')} <span>{t('customers')}</span>
                    </h2>
                </div>
                <div className="">
                    <div className="">
                        <div className="item">
                            <div className="box">
                                <div className="img-box">
                                    <img src="images/client1.jpg" alt="" className="box-img" />
                                </div>
                                <div className="detail-box">
                                    <div className="client_id">
                                        <div className="client_info">
                                            <h6>
                                                Ruben Benitah
                                            </h6>
                                            <p>
                                                responsable au Windsor
                                            </p>
                                        </div>
                                        <i className="fa fa-quote-left" aria-hidden="true"></i>
                                    </div>
                                    <p>
                                        Service au point de la technologie, tres bonne equipe
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="item">
                            <div className="box">
                                <div className="img-box">
                                    <img src="images/client2.jpg" alt="" className="box-img" />
                                </div>
                                <div className="detail-box">
                                    <div className="client_id">
                                        <div className="client_info">
                                            <h6>
                                                Zoe Vailllancourt
                                            </h6>
                                            <p>
                                               HR chez Deloitte
                                            </p>
                                        </div>
                                        <i className="fa fa-quote-left" aria-hidden="true"></i>
                                    </div>
                                    <p>
                                        J'ai plusieurs fois soliciter les services de 2lazy2work, et à chaque fois c'etait impecable    
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}

export default Client;